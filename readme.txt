This program was developed to help to study English words. The idea is to provide a simple and quick way to translate and store unknown word once the word appears in the Windows clipboard.

Contents:

1. TrayApp - WPF desktop application. 

Once started, the program appears in the system tray. The program monitors the clipboard and tries to translate clipboard content from English to Russian
Also, it is possible to call interactive translator window using Shift+Ctrl+T shortcut.

2. Translator.Lingvo.Server - WebAPI self-host application which could be installed as a Windows service or run as a console application. 

The application provides translation API for the TrayApp, queries ABBYY Lingvo service to get translations, and caches results in the SQLite DB