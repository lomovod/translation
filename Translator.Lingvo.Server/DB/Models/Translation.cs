﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Translator.Lingvo.Server.DB.Models
{
    [Table("Translations")]
    public class Translation
    {
        [Key]
        public int TranslationId { get; set; }

        public string Data { get; set; }

        public int WordId { get; set; }
        public Word Word { get; set; }
    }
}