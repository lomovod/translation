﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Translator.Lingvo.Server.DB.Models
{
    [Table("Words")]
    public class Word
    {
        [Key]
        public int WordId { get; set; }

        [Index]
        [MaxLength(32)]
        public string Data { get; set; }

        public List<Translation> Translations { get; set; }
    }
}