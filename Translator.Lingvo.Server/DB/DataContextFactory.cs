﻿using System.IO;
using Translator.Common.Services;

namespace Translator.Lingvo.Server.DB
{
    public class DataContextFactory : IDataContextFactory
    {
        private readonly string _databasePath;

        public DataContextFactory(IFolderService folderService)
        {
            var appFolder = folderService.GetAppFolder("DB");
            _databasePath = Path.Combine(appFolder, "cache.db");
        }

        public IDataContext Create()
        {
            var dataContext = new DataContext(_databasePath);
            dataContext.Database.EnsureCreated();
            return dataContext;
        }
    }
}