﻿using Microsoft.EntityFrameworkCore;
using Translator.Lingvo.Server.DB.Models;

namespace Translator.Lingvo.Server.DB
{
    public class DataContext : DbContext, IDataContext
    {
        private readonly string _databasePath;

        public DataContext(string databasePath)
        {
            _databasePath = databasePath;
        }

        public DbSet<Word> Words { get; set; }
        public DbSet<Translation> Translations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source={_databasePath}");
        }
    }
}