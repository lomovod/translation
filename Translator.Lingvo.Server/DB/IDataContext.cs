﻿using System;
using Microsoft.EntityFrameworkCore;
using Translator.Lingvo.Server.DB.Models;

namespace Translator.Lingvo.Server.DB
{
    public interface IDataContext : IDisposable
    {
        DbSet<Word> Words { get; }
        DbSet<Translation> Translations { get; }

        int SaveChanges();
    }
}