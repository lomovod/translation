﻿namespace Translator.Lingvo.Server.DB
{
    public interface IDataContextFactory
    {
        IDataContext Create();
    }
}