﻿using System.Reflection;
using System.Web.Http.SelfHost;
using Autofac;
using Autofac.Integration.WebApi;
using Translator.Lingvo.Server.Autofac.Modules;

namespace Translator.Lingvo.Server.Autofac
{
    public static class Bootstrapper
    {
        static Bootstrapper()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule(new MainModule());

            containerBuilder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            Container = containerBuilder.Build();
        }

        public static IContainer Container { get; }
    }
}