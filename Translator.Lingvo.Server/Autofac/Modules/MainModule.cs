﻿using Autofac;
using Translator.Common.Services;
using Translator.Lingvo.Server.DB;
using Translator.Lingvo.Server.Services;

namespace Translator.Lingvo.Server.Autofac.Modules
{
    public class MainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<LingvoTranslationService>().As<ILingvoTranslationService>().SingleInstance();

            builder.RegisterType<DataContextFactory>().As<IDataContextFactory>().SingleInstance();

            builder.Register(context => context.Resolve<IDataContextFactory>().Create())
                .As<IDataContext>()
                .InstancePerRequest();

            builder.RegisterType<LocalCachePersistenceService>().As<ILocalCachePersistenceService>().InstancePerRequest();
            builder.RegisterType<TranslationAggregator>().As<ITranslationAggregator>().InstancePerRequest();
            
            builder.RegisterType<FolderService>().As<IFolderService>().SingleInstance();
        }
    }
}