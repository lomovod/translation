﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using NLog;
using Translator.Api;
using Translator.Lingvo.Server.Services;

namespace Translator.Lingvo.Server.Controllers
{
    public class TranslationController : ApiController
    {
        private readonly ITranslationAggregator _translationAggregator;
        private static readonly Logger Logger = LogManager.GetLogger(nameof(TranslationController));

        public TranslationController(ITranslationAggregator translationAggregator)
        {
            _translationAggregator = translationAggregator;
        }

        [HttpGet]
        [Route("translate/{word}")]
        public async Task<TranslationResults> Translate(string word)
        {
            Logger.Info($"Translating {word}");
            var translationResults = await _translationAggregator.Translate(word);

            if (translationResults.Translations.Any())
            {
                Logger.Info($"Translations for {word} are: {string.Join(", ", translationResults.Translations)}");
            }
            else
                Logger.Info($"Unable to thanslate {word}");

            return translationResults;
        }
    }
}