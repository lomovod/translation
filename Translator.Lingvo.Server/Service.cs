﻿using System;
using System.Web.Http;
using System.Web.Http.SelfHost;
using Autofac.Integration.WebApi;
using NLog;
using Translator.Lingvo.Server.Autofac;

namespace Translator.Lingvo.Server
{
    public class Service : IDisposable
    {
        private const string BaseAddress = "http://127.0.0.1:5454";
        private static readonly Logger Logger = LogManager.GetLogger(nameof(Service));

        private readonly HttpSelfHostServer _server;
        private bool _started;

        public Service()
        {
            var selfHostConfiguraiton = new HttpSelfHostConfiguration(BaseAddress);

            selfHostConfiguraiton.MapHttpAttributeRoutes();
            selfHostConfiguraiton.DependencyResolver = new AutofacWebApiDependencyResolver(Bootstrapper.Container);

            _server = new HttpSelfHostServer(selfHostConfiguraiton);
        }

        public void Dispose()
        {
            _server.Dispose();
        }

        public async void Start()
        {
            if (_started)
                return;

            await _server.OpenAsync();
            _started = true;
            Logger.Info("Service started");
        }

        public async void Stop()
        {
            if (!_started)
                return;

            await _server.CloseAsync();
            _started = false;
            Logger.Info("Service stopped");
        }
    }
}