﻿using System;
using System.Linq;
using NLog;
using NLog.Config;
using NLog.Targets;
using Topshelf;

namespace Translator.Lingvo.Server
{
    internal class Program
    {
        private static void Main()
        {
            InitializeLogger();

            if (Environment.UserInteractive && !Environment.GetCommandLineArgs().Any())
                RunAsConsole();
            else
                RunAsService();
        }

        private static void RunAsService()
        {
            HostFactory.Run(x =>
            {
                x.Service<Service>(s =>
                {
                    s.ConstructUsing(name => new Service());
                    s.WhenStarted(svc => svc.Start());
                    s.WhenStopped(svc => svc.Stop());
                });

                x.RunAsLocalSystem();
                x.SetDescription("TranslationController Service which is used ABBYY Lingvo API");
                x.SetDisplayName("TranslationController Service (ABBYY Lingvo)");
                x.SetServiceName("TranslatorService");
            });
        }

        private static void RunAsConsole()
        {
            using (var service = new Service())
            {
                var logger = LogManager.GetLogger(nameof(Program));

                logger.Info("Trying to start the service");
                service.Start();

                logger.Info("Awaiting for the key to exit");
                Console.ReadKey();

                logger.Info("Exiting the service");
                service.Stop();
            }
        }

        private static void InitializeLogger()
        {
            var config = new LoggingConfiguration();
            var coloredConsoleTarget = new ColoredConsoleTarget();
            config.AddTarget("console", coloredConsoleTarget);
            var rule = new LoggingRule("*", LogLevel.Trace, coloredConsoleTarget);
            config.LoggingRules.Add(rule);

            LogManager.Configuration = config;
        }
    }
}