﻿using System.Threading.Tasks;
using Translator.Api;

namespace Translator.Lingvo.Server.Services
{
    public interface ILingvoTranslationService
    {
        Task<TranslationResults> Translate(string word);
    }
}