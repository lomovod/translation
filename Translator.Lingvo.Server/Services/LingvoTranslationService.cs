﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Translator.Api;

namespace Translator.Lingvo.Server.Services
{
    public class LingvoTranslationService : ILingvoTranslationService
    {
        private const string LingvoLiveHost = "https://developers.lingvolive.com";
        private const string AuthorizationHeader = "Authorization";

        private const string AuthToken =
            "MjlhNWI5ZTUtMDM2MS00M2I2LThiYjQtYzhlN2NlZDcwYjJlOjE4ZTZiM2VmMDNkYTRmNTBiYjZlNzRiM2U0YzMyODlk";

        private const string AuthenticateRequest = @"api/v1.1/authenticate";
        private const string MinicardRequest = "api/v1/Minicard?text={0}&srcLang=1033&dstLang=1049";

        private string _token;

        static LingvoTranslationService()
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;
        }

        public async Task<TranslationResults> Translate(string word)
        {
            if (_token == null)
                _token = await GetToken();

            if (_token == null)
                return new TranslationResults(new string[0]);

            return await GetTranslationResult(word);
        }

        private static async Task<string> GetToken()
        {
            using (var httpClient = CreateHttpClient())
            {
                httpClient.DefaultRequestHeaders.Add(AuthorizationHeader, $"Basic {AuthToken}");

                var responseMessage = await httpClient.PostAsJsonAsync(AuthenticateRequest, string.Empty);
                if (!responseMessage.IsSuccessStatusCode)
                    return null;

                return await responseMessage.Content.ReadAsStringAsync();
            }
        }

        private static HttpClient CreateHttpClient()
        {
            var httpClient = new HttpClient {BaseAddress = new Uri(LingvoLiveHost)};
            httpClient.DefaultRequestHeaders.Clear();
            return httpClient;
        }

        private async Task<TranslationResults> GetTranslationResult(string word)
        {
            using (var httpClient = CreateHttpClient())
            {
                httpClient.DefaultRequestHeaders.Add(AuthorizationHeader, $"Bearer {_token}");
                var responseMessage = await httpClient.GetAsync(string.Format(MinicardRequest, word));
                if (!responseMessage.IsSuccessStatusCode)
                    return new TranslationResults(new string[0]);

                var response = await responseMessage.Content.ReadAsStringAsync();
                return ParseTranslationResult(response);
            }
        }

        private static TranslationResults ParseTranslationResult(string response)
        {
            var jObject = JObject.Parse(response);
            var jToken = (string) jObject["Translation"]["Translation"];
            return new TranslationResults(jToken.Split(',').Select(s => s.Trim()).ToArray());
        }
    }
}