﻿using Translator.Common.Services;

namespace Translator.Lingvo.Server.Services
{
    public class FolderService : FolderServiceBase
    {
        protected override string RootAppFolder => "TranslationServer";
    }
}