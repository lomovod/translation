﻿using System.Threading.Tasks;
using Translator.Api;

namespace Translator.Lingvo.Server.Services
{
    public interface ILocalCachePersistenceService
    {
        void Persist(string word, TranslationResults translationResults);

        Task<TranslationResults> TryTranslateFromCache(string word);
    }
}