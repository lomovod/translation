﻿using System.Threading.Tasks;
using Translator.Api;

namespace Translator.Lingvo.Server.Services
{
    public interface ITranslationAggregator
    {
        Task<TranslationResults> Translate(string word);
    }
}