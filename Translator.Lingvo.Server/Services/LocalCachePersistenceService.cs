﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Translator.Api;
using Translator.Lingvo.Server.DB;
using Translator.Lingvo.Server.DB.Models;

namespace Translator.Lingvo.Server.Services
{
    public class LocalCachePersistenceService : ILocalCachePersistenceService
    {
        private readonly IDataContextFactory _dataContextFactory;

        public LocalCachePersistenceService(IDataContextFactory dataContextFactory)
        {
            _dataContextFactory = dataContextFactory;
        }

        public async void Persist(string word, TranslationResults translationResults)
        {
            await Task.Factory.StartNew(() =>
            {
                using (var dataContext = _dataContextFactory.Create())
                {
                    var entity = CreateEntity(word.ToLower(), translationResults);
                    dataContext.Words.Add(entity);
                    dataContext.SaveChanges();
                }
            });
        }

        public async Task<TranslationResults> TryTranslateFromCache(string word)
        {
            var lower = word.ToLower();

            using (var dataContext = _dataContextFactory.Create())
            {
                var result = await dataContext
                    .Words
                    .Include(word1 => word1.Translations)
                    .FirstOrDefaultAsync(w => w.Data == lower);

                var translations = result?.Translations.Select(translation => translation.Data).ToArray() ??
                                   new string[0];
                return new TranslationResults(translations);
            }
        }

        private static Word CreateEntity(string word, TranslationResults translationResults)
        {
            return new Word
            {
                Data = word,
                Translations =
                    GetTranslationsFromResult(translationResults)
            };
        }

        private static List<Translation> GetTranslationsFromResult(TranslationResults translationResults)
        {
            return new List<Translation>(translationResults.Translations.Select(CreateTranslation()));
        }

        private static Func<string, Translation> CreateTranslation()
        {
            return s => new Translation {Data = s};
        }
    }
}