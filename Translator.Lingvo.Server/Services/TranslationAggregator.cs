﻿using System.Linq;
using System.Threading.Tasks;
using NLog;
using Translator.Api;

namespace Translator.Lingvo.Server.Services
{
    public class TranslationAggregator : ITranslationAggregator
    {
        private readonly ILocalCachePersistenceService _localCachePersistenceService;
        private readonly ILingvoTranslationService _lingvoTranslationService;
        private static readonly Logger Logger = LogManager.GetLogger(nameof(TranslationAggregator));

        public TranslationAggregator(ILocalCachePersistenceService localCachePersistenceService, ILingvoTranslationService lingvoTranslationService)
        {
            _localCachePersistenceService = localCachePersistenceService;
            _lingvoTranslationService = lingvoTranslationService;
        }

        public async Task<TranslationResults> Translate(string word)
        {
            Logger.Info($"Trying to extract {word} from the local cache");
            var translation = await _localCachePersistenceService.TryTranslateFromCache(word);

            if (translation.Translations.Any())
            {
                Logger.Info($"The word {word} has been extracted from the cache");
                return translation;
            }

            Logger.Info($"Trying to get translation from the Server");
            translation = await _lingvoTranslationService.Translate(word);
            if (translation.Translations.Any())
            {
                Logger.Info($"The word has been translated, persisting in the local cache");
                _localCachePersistenceService.Persist(word, translation);
            }

            return translation;
        }
    }
}