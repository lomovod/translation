﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace TrayApp.Converters
{
    public class LoadingErrorTextMultiValueConverter : IMultiValueConverter
    {
        public static readonly LoadingErrorTextMultiValueConverter Instance = new LoadingErrorTextMultiValueConverter();

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var isLoading = values[0] as bool? ?? false;
            var errorText = values[1] as string;

            return !isLoading && !string.IsNullOrEmpty(errorText) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[0];
        }
    }
}