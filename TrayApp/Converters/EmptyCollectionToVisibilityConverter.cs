﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace TrayApp.Converters
{
    public class EmptyCollectionToVisibilityConverter : IValueConverter
    {
        public static readonly EmptyCollectionToVisibilityConverter Instance =
            new EmptyCollectionToVisibilityConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var enumerable = value as IEnumerable<object>;
            if (enumerable == null)
                return Visibility.Collapsed;
            return enumerable.Any() ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}