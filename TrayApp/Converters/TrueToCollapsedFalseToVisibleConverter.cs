﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace TrayApp.Converters
{
    public class TrueToCollapsedFalseToVisibleConverter : IValueConverter
    {
        public static readonly TrueToCollapsedFalseToVisibleConverter Instance = new TrueToCollapsedFalseToVisibleConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var boolValue = (value as bool?) ?? false;
            return boolValue ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}