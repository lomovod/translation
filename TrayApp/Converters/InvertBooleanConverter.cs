﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace TrayApp.Converters
{
    public class InvertBooleanConverter : IValueConverter
    {
        public static readonly InvertBooleanConverter Instance = new InvertBooleanConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return InvertBool(value);
        }

        private static object InvertBool(object value)
        {
            var boolValue = (bool) value;
            return !boolValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return InvertBool(value);
        }
    }
}