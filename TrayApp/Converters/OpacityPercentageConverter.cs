﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace TrayApp.Converters
{
    public class OpacityPercentageConverter : IValueConverter
    {
        public static readonly OpacityPercentageConverter Instance = new OpacityPercentageConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!double.TryParse(value?.ToString(), out var result))
                return 1;

            return result / 100;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}