﻿using System.Windows;
using System.Windows.Interop;
using TrayApp.Win32;

namespace TrayApp.Controls
{
    public class ExtendedWindow : Window
    {
        public static readonly DependencyProperty IsTransparentToMouseProperty = DependencyProperty.Register(
            "IsTransparentToMouse", typeof(bool), typeof(ExtendedWindow), new PropertyMetadata(default(bool), IsTransparentToMouseCallback));

        private static void IsTransparentToMouseCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var window = (Window) d;
            var isTransparent = (bool) e.NewValue;

            var hwnd = new WindowInteropHelper(window).Handle;

            var extendedStyle = WindowApi.GetWindowLong(hwnd, WindowApi.GWL_EXSTYLE);

            var newStyle = isTransparent
                ? extendedStyle | WindowApi.WS_EX_TRANSPARENT
                : extendedStyle & ~WindowApi.WS_EX_TRANSPARENT;

            WindowApi.SetWindowLong(hwnd, WindowApi.GWL_EXSTYLE, newStyle);
        }

        public bool IsTransparentToMouse
        {
            get => (bool) GetValue(IsTransparentToMouseProperty);
            set => SetValue(IsTransparentToMouseProperty, value);
        }
    }
}