﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TrayApp.Controls
{
    public class ImageCheckBox : CheckBox
    {
        public static readonly DependencyProperty CheckedSourceProperty = DependencyProperty.Register(
            "CheckedSource", typeof(ImageSource), typeof(ImageCheckBox), new PropertyMetadata(default(ImageSource)));

        public ImageSource CheckedSource
        {
            get => (ImageSource) GetValue(CheckedSourceProperty);
            set => SetValue(CheckedSourceProperty, value);
        }

        public static readonly DependencyProperty UncheckedSourceProperty = DependencyProperty.Register(
            "UncheckedSource", typeof(ImageSource), typeof(ImageCheckBox), new PropertyMetadata(default(ImageSource)));

        public ImageSource UncheckedSource
        {
            get => (ImageSource) GetValue(UncheckedSourceProperty);
            set => SetValue(UncheckedSourceProperty, value);
        }
    }
}