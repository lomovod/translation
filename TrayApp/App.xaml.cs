﻿using System.Windows;
using Autofac;
using TrayApp.Autofac;
using TrayApp.ViewModels;
using TrayApp.Views;

namespace TrayApp
{
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            var container = Bootstrapper.Container;
            var mainWindowViewModel = container.Resolve<IMainWindowViewModel>();

            var mainWindow = new MainWindow {DataContext = mainWindowViewModel};

            mainWindow.Show();
        }
    }
}