﻿namespace TrayApp.Services
{
    public interface IShutdownService
    {
        void Shutdown();
    }
}