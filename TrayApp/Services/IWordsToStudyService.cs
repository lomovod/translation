﻿namespace TrayApp.Services
{
    public interface IWordsToStudyService
    {
        void AddWord(string word, string translation);
    }
}