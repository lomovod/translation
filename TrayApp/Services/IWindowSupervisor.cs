﻿using TrayApp.Services.Windows;

namespace TrayApp.Services
{
    public interface IWindowSupervisor
    {
        IWindow<TViewModel> CreateWindow<TViewModel>(TViewModel windowViewModel);
    }
}