﻿using System.Collections.Generic;
using TrayApp.Domain;

namespace TrayApp.Services
{
    public interface IWordsToStudyRepository
    {
        IEnumerable<IWordToStudy> WordsToStudy { get; }
    }
}