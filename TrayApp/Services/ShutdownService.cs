﻿using System.Windows;

namespace TrayApp.Services
{
    internal class ShutdownService : IShutdownService
    {
        public void Shutdown()
        {
            Application.Current.Shutdown();
        }
    }
}