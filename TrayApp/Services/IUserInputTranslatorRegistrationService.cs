﻿namespace TrayApp.Services
{
    public interface IUserInputTranslatorRegistrationService
    {
        void Register();
    }
}