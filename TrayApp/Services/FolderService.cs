﻿using Translator.Common.Services;

namespace TrayApp.Services
{
    public class FolderService : FolderServiceBase
    {
        protected override string RootAppFolder => "Translation";
    }
}