﻿namespace TrayApp.Services
{
    public interface ITranslatorWindowManager
    {
        void ShowTranslation(string wordToTranslate);
    }
}