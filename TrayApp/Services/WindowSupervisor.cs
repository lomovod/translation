﻿using TrayApp.Autofac.Services;
using TrayApp.Services.Windows;

namespace TrayApp.Services
{
    public class WindowSupervisor : IWindowSupervisor
    {
        private readonly IResolver _resolver;

        public WindowSupervisor(IResolver resolver)
        {
            _resolver = resolver;
        }

        public IWindow<TViewModel> CreateWindow<TViewModel>(TViewModel windowViewModel)
        {
            var window = _resolver.Resolve<IWindow<TViewModel>>();
            window.DataContext = windowViewModel;

            return window;
        }
    }
}