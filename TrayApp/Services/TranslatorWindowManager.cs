﻿using System;
using TrayApp.Services.Persistence.Settings;
using TrayApp.ViewModels;

namespace TrayApp.Services
{
    public class TranslatorWindowManager : ITranslatorWindowManager, IDisposable
    {
        private readonly IWindowManager<TranslationWindowViewModelSettings, ITranslationWindowViewModel>
            _translationWindowManager;

        public TranslatorWindowManager(
            IWindowManager<TranslationWindowViewModelSettings, ITranslationWindowViewModel> translationWindowManager)
        {
            _translationWindowManager = translationWindowManager;
        }

        public void Dispose()
        {
            _translationWindowManager.TryClose();
        }

        public void ShowTranslation(string wordToTranslate)
        {
            _translationWindowManager.Open();
            if (_translationWindowManager.ViewModel != null)
                _translationWindowManager.ViewModel.WordToTranslate = wordToTranslate;
        }
    }
}