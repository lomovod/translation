﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TrayApp.Domain;
using TrayApp.Services.Persistence;
using TrayApp.Services.Persistence.Settings;

namespace TrayApp.Services
{
    internal class WordsToStudyRepository : IWordsToStudyRepository, IWordsToStudyService
    {
        private readonly ISettingsPersistenceService<WordsToStudySettings> _settingsPersistenceService;
        private readonly ObservableCollection<IWordToStudy> _wordsToStudy;
        private readonly IWordToStudyFactory _wordToStudyFactory;

        public WordsToStudyRepository(IWordToStudyFactory wordToStudyFactory,
            ISettingsPersistenceService<WordsToStudySettings> settingsPersistenceService)
        {
            _wordToStudyFactory = wordToStudyFactory;
            _settingsPersistenceService = settingsPersistenceService;

            var wordsFromSettings = GetWordsFromSettings();
            _wordsToStudy = new ObservableCollection<IWordToStudy>(wordsFromSettings);
        }

        public IEnumerable<IWordToStudy> WordsToStudy => _wordsToStudy;

        public void AddWord(string word, string translation)
        {
            var wordToStudyInList =
                _wordsToStudy.FirstOrDefault(
                    study => string.Equals(study.Word, word, StringComparison.InvariantCultureIgnoreCase));
            var newWordToStudy = wordToStudyInList == null
                ? _wordToStudyFactory.Create(word, translation)
                : _wordToStudyFactory.Create(wordToStudyInList, translation);

            if (wordToStudyInList != null)
                _wordsToStudy.Remove(wordToStudyInList);
            _wordsToStudy.Insert(0, newWordToStudy);

            PersistWords();
        }

        private void PersistWords()
        {
            var words =
                _wordsToStudy.Select(CreateWordSetting);
            _settingsPersistenceService.Settings.Words = words.ToArray();
        }

        private IEnumerable<IWordToStudy> GetWordsFromSettings()
        {
            return _settingsPersistenceService.Settings.Words.Select(CreateWordToStudy);
        }

        private static WordSetting CreateWordSetting(IWordToStudy study)
        {
            return new WordSetting
            {
                Word = study.Word,
                Translations = study.Translations,
                RepeatCount = study.RepeatCount
            };
        }

        private IWordToStudy CreateWordToStudy(WordSetting setting)
        {
            return _wordToStudyFactory.Create(setting.Word, setting.Translations, setting.RepeatCount);
        }
    }
}