﻿using System;
using System.Windows.Input;

namespace TrayApp.Services
{
    public class HotKey : IHotKey
    {
        public HotKey(Key k, KeyModifier keyModifiers)
        {
            Key = k;
            KeyModifiers = keyModifiers;
        }

        public Key Key { get; }
        public KeyModifier KeyModifiers { get; }
        public event Action<HotKey> Action;

        public void InvokeEvent()
        {
            Action?.Invoke(this);
        }
    }
}