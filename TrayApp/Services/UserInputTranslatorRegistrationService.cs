﻿using System;
using System.Windows.Input;
using TrayApp.Services.Persistence.Settings;
using TrayApp.ViewModels;

namespace TrayApp.Services
{
    public class UserInputTranslatorRegistrationService : IUserInputTranslatorRegistrationService, IDisposable
    {
        private readonly IGlobalHotkeyService _globalHotkeyService;

        private readonly IWindowManager<UserInputTranslatorViewModelSettings, IUserInputTranslatorViewModel>
            _windowManager;

        private IHotKey _hotKey;

        public UserInputTranslatorRegistrationService(
            IWindowManager<UserInputTranslatorViewModelSettings, IUserInputTranslatorViewModel> windowManager,
            IGlobalHotkeyService globalHotkeyService)
        {
            _windowManager = windowManager;
            _globalHotkeyService = globalHotkeyService;
        }

        public void Register()
        {
            if(_hotKey != null)
                return;

            _hotKey = _globalHotkeyService.Register(Key.T, KeyModifier.Shift | KeyModifier.Ctrl);
            _hotKey.Action += OnShowWindow;
        }

        private void OnShowWindow(HotKey hotKey)
        {
            _windowManager.Open();
        }

        public void Dispose()
        {
            if (_hotKey != null)
            {
                _hotKey.Action -= OnShowWindow;
                _globalHotkeyService.Unregister(_hotKey);
            }

            _windowManager?.Dispose();
        }
    }
}