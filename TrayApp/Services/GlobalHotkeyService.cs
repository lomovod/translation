﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Interop;
using TrayApp.Win32;

namespace TrayApp.Services
{
    public class GlobalHotkeyService : IGlobalHotkeyService, IDisposable
    {
        private const int WmHotKey = 0x0312;
        private readonly Dictionary<int, HotKey> _dictHotKeyToCalBackProc = new Dictionary<int, HotKey>();

        public GlobalHotkeyService()
        {
            ComponentDispatcher.ThreadFilterMessage += ComponentDispatcherThreadFilterMessage;
        }

        public void Dispose()
        {
            foreach (var hotKey in _dictHotKeyToCalBackProc.AsEnumerable().ToArray())
                Unregister(hotKey.Value);
            ComponentDispatcher.ThreadFilterMessage -= ComponentDispatcherThreadFilterMessage;
        }

        public IHotKey Register(Key k, KeyModifier keyModifiers)
        {
            var virtualKeyCode = CalculateVirtualKeyCode(k);
            var id = CalculateId(keyModifiers, virtualKeyCode);
            if (_dictHotKeyToCalBackProc.TryGetValue(virtualKeyCode, out HotKey hotKey))
                return hotKey;

            var key = new HotKey(k, keyModifiers);
            WindowApi.RegisterHotKey(IntPtr.Zero, id, (uint) keyModifiers, (uint) virtualKeyCode);
            _dictHotKeyToCalBackProc.Add(id, key);

            return key;
        }

        public void Unregister(IHotKey hotKey)
        {
            var virtualKeyCode = CalculateVirtualKeyCode(hotKey.Key);
            var id = CalculateId(hotKey.KeyModifiers, virtualKeyCode);

            if (!_dictHotKeyToCalBackProc.TryGetValue(id, out HotKey _))
                return;

            WindowApi.UnregisterHotKey(IntPtr.Zero, id);
            _dictHotKeyToCalBackProc.Remove(id);
        }

        private static int CalculateVirtualKeyCode(Key k)
        {
            return KeyInterop.VirtualKeyFromKey(k);
        }

        private static int CalculateId(KeyModifier keyModifiers, int virtualKeyCode)
        {
            return virtualKeyCode + (int) keyModifiers * 0x10000;
        }

        private void ComponentDispatcherThreadFilterMessage(ref MSG msg, ref bool handled)
        {
            if (!_dictHotKeyToCalBackProc.Any())
                return;

            if (handled) return;

            if (msg.message != WmHotKey) return;

            if (!_dictHotKeyToCalBackProc.TryGetValue((int) msg.wParam, out HotKey hotKey)) return;

            hotKey.InvokeEvent();
            handled = true;
        }
    }
}