﻿using System;

namespace TrayApp.Services
{
    public interface IWindowManager<TSettings, out TViewModel> : IDisposable
    {
        void Open();

        void TryClose();

        event Action WindowClosed;

        TViewModel ViewModel { get; }
    }
}