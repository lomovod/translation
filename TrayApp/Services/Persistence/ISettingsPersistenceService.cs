﻿using System;

namespace TrayApp.Services.Persistence
{
    public interface ISettingsPersistenceService<out TSettings> : IDisposable, ISettingsStorage
    {
        TSettings Settings { get; }
    }
}