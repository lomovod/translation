﻿namespace TrayApp.Services.Persistence
{
    public interface ISaveAllSettingsService
    {
        void SaveAll();
    }
}