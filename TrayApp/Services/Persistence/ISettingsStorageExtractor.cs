﻿using System;

namespace TrayApp.Services.Persistence
{
    public interface ISettingsStorageExtractor
    {
        ISettingsStorage Get(Type settingsType);
    }
}