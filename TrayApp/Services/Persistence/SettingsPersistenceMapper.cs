﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Translator.Common.Services;
using TrayApp.Services.Persistence.Settings;

namespace TrayApp.Services.Persistence
{
    public class SettingsPersistenceMapper : ISettingsPersistenceMapper
    {
        private readonly IFolderService _folderService;
        private readonly IDictionary<Type, string> _map = new Dictionary<Type, string>();

        public SettingsPersistenceMapper(IFolderService folderService)
        {
            _folderService = folderService;

            BuildMap();
        }

        public string GetSettingsPath<TSettings>()
        {
            var path = _map[typeof(TSettings)];
            if (string.IsNullOrEmpty(path))
                return null;

            var splittedPath = path.Split('\\');
            var subpath = splittedPath.Take(splittedPath.Length - 1);
            var fileName = splittedPath[splittedPath.Length - 1];

            var settingsFilePath = string.Join("\\", subpath);
            var appFolder = _folderService.GetAppFolder(settingsFilePath);
            return Path.Combine(appFolder, fileName);
        }

        public IEnumerable<Type> GetAllSettingsTypes()
        {
            return _map.Keys;
        }

        private void BuildMap()
        {
            _map.Add(typeof(TranslationWindowViewModelSettings), @"Windows\TranslationWindow.json");
            _map.Add(typeof(WordsToStudyViewModelSettings), @"Windows\WordsToStudyWindow.json");
            _map.Add(typeof(WordsToStudySettings), @"Windows\WordsToStudy.json");
            _map.Add(typeof(UserInputTranslatorViewModelSettings), @"Windows\UserInputTranslationWindow.json");
            _map.Add(typeof(GeneralSettings), @"Windows\GeneralSettings.json");
        }
    }
}