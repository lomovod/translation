﻿using System.IO;
using Newtonsoft.Json;
using TrayApp.Services.Persistence.Settings;

namespace TrayApp.Services.Persistence
{
    public class SettingsPersistenceService<TSettings> : ISettingsPersistenceService<TSettings> where TSettings : new()
    {
        private readonly ISettingsPersistenceMapper _mapper;

        public SettingsPersistenceService(ISettingsPersistenceMapper mapper)
        {
            _mapper = mapper;
            Load();
        }

        public void Load()
        {
            var settingsPath = _mapper.GetSettingsPath<TSettings>();
            if (!File.Exists(settingsPath))
            {
                Settings = new TSettings();
                return;
            }
            var text = File.ReadAllText(settingsPath);
            Settings = JsonConvert.DeserializeObject<TSettings>(text);
        }

        public void Save()
        {
            var settingsPath = _mapper.GetSettingsPath<TSettings>();

            var text = JsonConvert.SerializeObject(Settings, Formatting.Indented);
            File.WriteAllText(settingsPath, text);
        }

        public TSettings Settings { get; private set; }

        public void Dispose()
        {
            Save();
        }
    }
}