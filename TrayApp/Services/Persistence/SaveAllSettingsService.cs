﻿namespace TrayApp.Services.Persistence
{
    public class SaveAllSettingsService : ISaveAllSettingsService
    {
        private readonly ISettingsPersistenceMapper _mapper;
        private readonly ISettingsStorageExtractor _settingsStorageExtractor;

        public SaveAllSettingsService(ISettingsStorageExtractor settingsStorageExtractor,
            ISettingsPersistenceMapper mapper)
        {
            _settingsStorageExtractor = settingsStorageExtractor;
            _mapper = mapper;
        }

        public void SaveAll()
        {
            foreach (var type in _mapper.GetAllSettingsTypes())
            {
                var storage = _settingsStorageExtractor.Get(type);
                storage.Save();
            }
        }
    }
}