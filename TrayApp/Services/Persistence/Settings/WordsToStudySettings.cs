﻿namespace TrayApp.Services.Persistence.Settings
{
    public class WordsToStudySettings
    {
        public WordSetting[] Words { get; set; } = new WordSetting[0];
    }
}