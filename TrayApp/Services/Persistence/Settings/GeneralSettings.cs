﻿namespace TrayApp.Services.Persistence.Settings
{
    public class GeneralSettings
    {
        public bool HookClipboard { get; set; } = true;
    }
}