﻿namespace TrayApp.Services.Persistence.Settings
{
    public class WordSetting
    {
        public string Word { get; set; }
        public string[] Translations { get; set; }
        public int RepeatCount { get; set; } = 0;
    }
}