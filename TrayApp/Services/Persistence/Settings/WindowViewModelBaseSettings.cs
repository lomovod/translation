﻿namespace TrayApp.Services.Persistence.Settings
{
    public class WindowViewModelBaseSettings
    {
        public int OpacityPercentage { get; set; } = 100;
        public double Left { get; set; } = double.NaN;
        public double Top { get; set; } = double.NaN;
        public bool IsTransparent { get; set; } = false;
    }
}