﻿namespace TrayApp.Services.Persistence.Settings
{
    public class WordsToStudyViewModelSettings : WindowViewModelBaseSettings
    {
        public double Width { get; set; } = 300;
        public double Height { get; set; } = 300;
    }
}