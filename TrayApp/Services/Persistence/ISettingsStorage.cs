﻿namespace TrayApp.Services.Persistence
{
    public interface ISettingsStorage
    {
        void Load();
        void Save();
    }
}