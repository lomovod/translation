﻿using System;
using System.Collections.Generic;

namespace TrayApp.Services.Persistence
{
    public interface ISettingsPersistenceMapper
    {
        string GetSettingsPath<TSettings>();

        IEnumerable<Type> GetAllSettingsTypes();
    }
}