﻿using System;
using TrayApp.Autofac.Services;

namespace TrayApp.Services.Persistence
{
    public class SettingsStorageExtractor : ISettingsStorageExtractor
    {
        private readonly IResolver _resolver;

        public SettingsStorageExtractor(IResolver resolver)
        {
            _resolver = resolver;
        }

        public ISettingsStorage Get(Type settingsType)
        {
            return (ISettingsStorage) _resolver.Resolve(
                typeof(ISettingsPersistenceService<>).MakeGenericType(settingsType));
        }
    }
}