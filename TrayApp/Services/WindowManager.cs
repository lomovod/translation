﻿using System;
using TrayApp.Autofac.Services;
using TrayApp.Services.Persistence;
using TrayApp.Services.Windows;
using TrayApp.ViewModels.Common;

namespace TrayApp.Services
{
    public class WindowManager<TSettings, TViewModel> : IWindowManager<TSettings, TViewModel>
        where TViewModel : IDisposable, IClosableViewModel
    {
        private readonly IFactory<TSettings, TViewModel> _translatorViewModelFactory;
        private readonly ISettingsPersistenceService<TSettings> _windowSettings;
        private readonly IWindowSupervisor _windowSupervisor;
        private TViewModel _viewModel;
        private IWindow<TViewModel> _window;

        public WindowManager(IWindowSupervisor windowSupervisor,
            ISettingsPersistenceService<TSettings> windowSettings,
            IFactory<TSettings, TViewModel> translatorViewModelFactory)
        {
            _windowSupervisor = windowSupervisor;
            _windowSettings = windowSettings;
            _translatorViewModelFactory = translatorViewModelFactory;
        }

        public void Dispose()
        {
            TryClose();
        }

        public void Open()
        {
            if (_viewModel == null)
            {
                _viewModel = _translatorViewModelFactory.Create(_windowSettings.Settings);

                _window = _windowSupervisor.CreateWindow(_viewModel);
                _window.WindowClosed += OnWindowClosed;

                _viewModel.Closed += OnViewModelClosed;

                _window.Show();
            }
        }

        public void TryClose()
        {
            _window?.Close();
        }

        public event Action WindowClosed;
        public TViewModel ViewModel => _viewModel;

        private void OnViewModelClosed()
        {
            _window.Close();
        }

        private void OnWindowClosed()
        {
            _viewModel.Closed -= OnViewModelClosed;
            _viewModel.Dispose();
            _viewModel = default(TViewModel);

            _window.WindowClosed -= OnWindowClosed;
            _window = null;

            WindowClosed?.Invoke();
        }
    }
}