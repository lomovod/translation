﻿using System;
using System.Windows.Input;

namespace TrayApp.Services
{
    public interface IHotKey
    {
        event Action<HotKey> Action;
        Key Key { get; }
        KeyModifier KeyModifiers { get; }
    }
}