﻿using System.Windows.Input;

namespace TrayApp.Services
{
    public interface IGlobalHotkeyService
    {
        IHotKey Register(Key k, KeyModifier keyModifiers);
        void Unregister(IHotKey hotKey);
    }
}