﻿using System;

namespace TrayApp.Services.Windows
{
    public interface IWindow<TViewModel>
    {
        object DataContext { get; set; }
        void Show();

        void Close();

        event Action WindowClosed;
    }
}