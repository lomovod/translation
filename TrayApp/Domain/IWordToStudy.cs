﻿namespace TrayApp.Domain
{
    public interface IWordToStudy
    {
        string[] Translations { get; }
        string Word { get; }
        int RepeatCount { get; }

        bool ShowRepeatCount { get; }
    }
}