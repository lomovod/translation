﻿namespace TrayApp.Domain
{
    public class WordToStudy : IWordToStudy
    {
        public WordToStudy(string word, string[] translations, int repeatCount = 0)
        {
            Word = word;
            Translations = translations;
            RepeatCount = repeatCount;
        }

        public string Word { get; }
        public int RepeatCount { get; }
        public bool ShowRepeatCount => RepeatCount > 0;
        public string[] Translations { get; }
    }
}