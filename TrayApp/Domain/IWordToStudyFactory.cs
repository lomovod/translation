﻿namespace TrayApp.Domain
{
    public interface IWordToStudyFactory
    {
        IWordToStudy Create(string word, string[] translations, int repeatCount);
        IWordToStudy Create(string word, string translation);
        IWordToStudy Create(IWordToStudy wordToStudy, string anotherTranslation);
    }
}