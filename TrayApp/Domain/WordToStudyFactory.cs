﻿using System;
using System.Globalization;
using System.Linq;

namespace TrayApp.Domain
{
    internal class WordToStudyFactory : IWordToStudyFactory
    {
        public IWordToStudy Create(string word, string[] translations, int repeatCount)
        {
            return new WordToStudy(word, translations, repeatCount);
        }

        public IWordToStudy Create(string word, string translation)
        {
            return Create(word, new[] {translation}, 0);
        }

        public IWordToStudy Create(IWordToStudy wordToStudy, string anotherTranslation)
        {
            var translations = wordToStudy.Translations.Concat(new[] {anotherTranslation})
                .Distinct(StringComparer.Create(CultureInfo.InvariantCulture, true));
            var newRepeatCount = wordToStudy.RepeatCount + 1;
            return Create(wordToStudy.Word, translations.ToArray(), newRepeatCount);
        }
    }
}