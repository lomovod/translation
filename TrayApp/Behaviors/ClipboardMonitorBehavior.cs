﻿using System;
using System.Windows;
using System.Windows.Interactivity;
using System.Windows.Interop;
using TrayApp.Win32;

namespace TrayApp.Behaviors
{
    public class ClipboardMonitorBehavior : Behavior<Window>
    {
        public static readonly DependencyProperty ClipboardMonitorProperty = DependencyProperty.Register(
            "ClipboardMonitor", typeof(IClipboardMonitor), typeof(ClipboardMonitorBehavior), new PropertyMetadata(default(IClipboardMonitor)));

        private IntPtr _windowHandle;
        private HwndSource _hwndSource;

        public IClipboardMonitor ClipboardMonitor
        {
            get => (IClipboardMonitor) GetValue(ClipboardMonitorProperty);
            set => SetValue(ClipboardMonitorProperty, value);
        }

        protected override void OnAttached()
        {
            _windowHandle = new WindowInteropHelper(AssociatedObject).EnsureHandle();
            _hwndSource = HwndSource.FromHwnd(_windowHandle);

            _hwndSource?.AddHook(WindowMessageHandler);

            WindowApi.AddClipboardFormatListener(_windowHandle);

            AssociatedObject.Closed += OnWindowClosed;
        }

        private void OnWindowClosed(object sender, EventArgs e)
        {
            WindowApi.RemoveClipboardFormatListener(_windowHandle);

            _hwndSource?.RemoveHook(WindowMessageHandler);
            _hwndSource?.Dispose();
        }

        private IntPtr WindowMessageHandler(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam, ref bool handled)
        {
            if (msg == WindowApi.WM_CLIPBOARDUPDATE)
            {
                var containsText = Clipboard.ContainsText();
                if (containsText && ClipboardMonitor != null)
                {
                    var clipboardText = Clipboard.GetText();
                    ClipboardMonitor.OnClipboardChanged(clipboardText);
                }
            }
            handled = false;
            return IntPtr.Zero;
        }
    }
}