﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace TrayApp.Behaviors
{
    public class MouseDownBehavior : Behavior<UIElement>
    {
        protected override void OnAttached()
        {
            AssociatedObject.MouseDown += OnMouseDown;
            AssociatedObject.MouseLeftButtonDown += OnMouseLeftButtonDown;
        }

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            
        }
    }
}