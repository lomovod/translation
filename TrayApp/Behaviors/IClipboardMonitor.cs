﻿namespace TrayApp.Behaviors
{
    public interface IClipboardMonitor
    {
        void OnClipboardChanged(string text);
    }
}