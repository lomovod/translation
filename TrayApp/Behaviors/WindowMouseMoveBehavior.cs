﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace TrayApp.Behaviors
{
    public class WindowMouseMoveBehavior : Behavior<Window>
    {
        protected override void OnAttached()
        {
            AssociatedObject.MouseDown += OnMouseDown;

            AssociatedObject.Closed += OnWindowClosed;
        }

        private void OnWindowClosed(object sender, EventArgs e)
        {
            AssociatedObject.MouseDown -= OnMouseDown;
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                AssociatedObject.DragMove();
        }
    }
}