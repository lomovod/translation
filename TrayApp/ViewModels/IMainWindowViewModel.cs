using System.Windows.Input;

namespace TrayApp.ViewModels
{
    public interface IMainWindowViewModel
    {
        ICommand ExitCommand { get; }

        bool IsWordsToStudyShown { get; set; }
        bool IsWordsToStudyTransparent { get; set; }

        bool IsClipboardHooked { get; set; }
    }
}