﻿using System.Collections.Generic;
using TrayApp.Domain;
using TrayApp.Services;
using TrayApp.Services.Persistence.Settings;
using TrayApp.ViewModels.Common;

namespace TrayApp.ViewModels
{
    internal class WordsToStudyViewModel : WindowViewModelBase, IWordsToStudyViewModel
    {
        private readonly WordsToStudyViewModelSettings _settings;
        private readonly IWordsToStudyRepository _wordsToStudyRepository;
        private bool _isTransparent;

        public WordsToStudyViewModel(WordsToStudyViewModelSettings settings,
            IWordsToStudyRepository wordsToStudyRepository) : base(settings)
        {
            _settings = settings;
            _wordsToStudyRepository = wordsToStudyRepository;
        }

        public void Dispose()
        {
        }

        public double Width
        {
            get => _settings.Width;
            set
            {
                if (_settings.Width == value)
                    return;
                _settings.Width = value;
                OnPropertyChanged();
            }
        }

        public double Height
        {
            get => _settings.Height;
            set
            {
                if (_settings.Height == value)
                    return;
                _settings.Height = value;
                OnPropertyChanged();
            }
        }

        public IEnumerable<IWordToStudy> Words => _wordsToStudyRepository.WordsToStudy;
    }
}