﻿namespace TrayApp.ViewModels
{
    public class TranslatedWordViewModel : ITranslatedWordViewModel
    {
        public TranslatedWordViewModel(string word)
        {
            Word = word;
        }

        public string Word { get; }
        public bool Clickable { get; } = true;
    }
}