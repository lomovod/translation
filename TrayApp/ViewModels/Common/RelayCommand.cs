﻿using System;
using System.Windows.Input;

namespace TrayApp.ViewModels.Common
{
    public class RelayCommand : AbstractCommand
    {
        private readonly Action _executeAction;
        private readonly Func<bool> _canExecuteAction;

        public RelayCommand(Action executeAction)
        {
            _executeAction = executeAction;
        }

        public RelayCommand(Action executeAction, Func<bool> canExecuteAction)
        {
            _executeAction = executeAction;
            _canExecuteAction = canExecuteAction;
        }

        public override bool CanExecute(object parameter)
        {
            return _canExecuteAction?.Invoke() ?? true;
        }

        public override void Execute(object parameter)
        {
            _executeAction.Invoke();
        }

        public void RaiseCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }
    }

    public class RelayCommand<T> : AbstractCommand
    {
        private readonly Action<T> _executeAction;

        public RelayCommand(Action<T> executeAction)
        {
            _executeAction = executeAction;
        }

        public override void Execute(object parameter)
        {
            _executeAction.Invoke((T) parameter);
        }
    }
}