﻿using System;

namespace TrayApp.ViewModels.Common
{
    public interface IClosableViewModel
    {
        event Action Closed;
    }
}