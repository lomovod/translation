﻿using System.Windows.Input;

namespace TrayApp.ViewModels.Common
{
    public interface IWindowViewModelBase : IClosableViewModel
    {
        int OpacityPercentage { get; set; }

        double Left { get; set; }

        double Top { get; set; }

        ICommand CloseCommand { get; }

        bool IsTransparent { get; set; }
    }
}