﻿using System;
using System.Windows.Input;
using TrayApp.Services.Persistence.Settings;

namespace TrayApp.ViewModels.Common
{
    public class WindowViewModelBase : NotificationObject, IWindowViewModelBase
    {
        protected readonly WindowViewModelBaseSettings Settings;
        private bool _isTransparent;

        protected WindowViewModelBase(WindowViewModelBaseSettings settings)
        {
            Settings = settings;
            CloseCommand = new RelayCommand(Close);
        }

        public int OpacityPercentage
        {
            get => Settings.OpacityPercentage;
            set
            {
                if (Settings.OpacityPercentage == value)
                    return;
                Settings.OpacityPercentage = value;
                OnPropertyChanged();
            }
        }

        public double Left
        {
            get => Settings.Left;
            set
            {
                Settings.Left = value;
                OnPropertyChanged();
            }
        }

        public double Top
        {
            get => Settings.Top;
            set
            {
                Settings.Top = value;
                OnPropertyChanged();
            }
        }

        public ICommand CloseCommand { get; }

        public event Action Closed;

        public bool IsTransparent
        {
            get => Settings.IsTransparent;
            set
            {
                Settings.IsTransparent = value;
                OnPropertyChanged();
            }
        }

        private void Close()
        {
            Closed?.Invoke();
        }
    }
}