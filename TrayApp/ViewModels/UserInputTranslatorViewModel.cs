﻿using System;
using System.Linq;
using System.Windows.Input;
using Translator.Api;
using TrayApp.Autofac.Services;
using TrayApp.Services;
using TrayApp.Services.Persistence.Settings;
using TrayApp.ViewModels.Common;

namespace TrayApp.ViewModels
{
    public class UserInputTranslatorViewModel : WindowViewModelBase, IUserInputTranslatorViewModel
    {
        private readonly RelayCommand _searchCommand;
        private readonly IFactory<string, ITranslatedWordViewModel> _translatedWordViewModelFactory;
        private readonly ITranslationClient _translationClient;
        private readonly IWordsToStudyService _wordsToStudyService;
        private bool _isLoading;
        private string _loadingError;
        private ITranslatedWordViewModel[] _translatedWords;
        private string _wordToTranslate;

        public UserInputTranslatorViewModel(UserInputTranslatorViewModelSettings settings,
            IWordsToStudyService wordsToStudyService,
            ITranslationClient translationClient,
            IFactory<string, ITranslatedWordViewModel> translatedWordViewModelFactory) : base(settings)
        {
            _wordsToStudyService = wordsToStudyService;
            _translationClient = translationClient;
            _translatedWordViewModelFactory = translatedWordViewModelFactory;
            _searchCommand = new RelayCommand(Search, CanSearch);
            AddWordToStudyListCommand = new RelayCommand<string>(AddWordToStudyList);
        }


        public void Dispose()
        {
        }

        public ITranslatedWordViewModel[] TranslatedWords
        {
            get => _translatedWords;
            private set
            {
                _translatedWords = value;
                OnPropertyChanged();
            }
        }

        public bool IsLoading
        {
            get => _isLoading;
            private set
            {
                if (_isLoading == value)
                    return;

                _isLoading = value;
                if (_isLoading)
                    LoadingError = null;

                OnPropertyChanged();
            }
        }

        public string LoadingError
        {
            get => _loadingError;
            private set
            {
                if (_loadingError == value)
                    return;
                _loadingError = value;
                OnPropertyChanged();
            }
        }

        public ICommand AddWordToStudyListCommand { get; }

        public string WordToTranslate
        {
            get => _wordToTranslate;
            set
            {
                if (_wordToTranslate == value)
                    return;
                _wordToTranslate = value;
                OnPropertyChanged();
                _searchCommand.RaiseCanExecuteChanged();
            }
        }

        public ICommand SearchCommand => _searchCommand;

        private bool CanSearch()
        {
            return !string.IsNullOrWhiteSpace(WordToTranslate);
        }

        private async void Translate(string word)
        {
            IsLoading = true;
            TranslatedWords = null;
            
            try
            {
                var translationResults = await _translationClient.TranslateAsync(word);
                if (translationResults == null)
                    throw new Exception("No translation");

                TranslatedWords = translationResults.Translations.Select(CreateTranslatedWordViewModel).ToArray();
            }
            catch (Exception ex)
            {
                LoadingError = ex.Message;
            }
            finally
            {
                IsLoading = false;
            }
        }

        private ITranslatedWordViewModel CreateTranslatedWordViewModel(string s)
        {
            return _translatedWordViewModelFactory.Create(s);
        }

        private void AddWordToStudyList(string translation)
        {
            _wordsToStudyService.AddWord(WordToTranslate, translation);
            CloseCommand.Execute(null);
        }

        private void Search()
        {
            Translate(WordToTranslate);
        }
    }
}