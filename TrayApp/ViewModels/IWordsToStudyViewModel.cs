﻿using System;
using System.Collections.Generic;
using TrayApp.Domain;
using TrayApp.ViewModels.Common;

namespace TrayApp.ViewModels
{
    public interface IWordsToStudyViewModel : IWindowViewModelBase, IDisposable
    {
        double Width { get; set; }
        double Height { get; set; }

        IEnumerable<IWordToStudy> Words { get; }
    }
}