﻿using System.Windows.Input;
using TrayApp.Behaviors;
using TrayApp.Services;
using TrayApp.Services.Persistence;
using TrayApp.Services.Persistence.Settings;
using TrayApp.ViewModels.Common;

namespace TrayApp.ViewModels
{
    public class MainWindowViewModel : NotificationObject, IMainWindowViewModel, IClipboardMonitor
    {
        private readonly ISettingsPersistenceService<GeneralSettings> _generalSettings;
        private readonly ISaveAllSettingsService _saveAllSettingsService;
        private readonly IShutdownService _shutdownService;
        private readonly ITranslatorWindowManager _translatorWindowManager;

        private readonly IWindowManager<WordsToStudyViewModelSettings, IWordsToStudyViewModel>
            _wordsToStudyWindowManager;

        private bool _isWordsToStudyShown = true;

        public MainWindowViewModel(IShutdownService shutdownService, ITranslatorWindowManager translatorWindowManager,
            ISaveAllSettingsService saveAllSettingsService,
            IWindowManager<WordsToStudyViewModelSettings, IWordsToStudyViewModel> wordsToStudyWindowManager,
            ISettingsPersistenceService<GeneralSettings> generalSettings)
        {
            _shutdownService = shutdownService;
            _translatorWindowManager = translatorWindowManager;
            _saveAllSettingsService = saveAllSettingsService;
            _wordsToStudyWindowManager = wordsToStudyWindowManager;
            _generalSettings = generalSettings;

            ExitCommand = new RelayCommand(OnExit);
            AdjustWordsToStudyWindowVisibility();
            _wordsToStudyWindowManager.WindowClosed += OnWordsToStudyWindowClosed;
        }

        public void OnClipboardChanged(string text)
        {
            if (IsClipboardHooked)
                _translatorWindowManager.ShowTranslation(text);
        }

        public ICommand ExitCommand { get; }

        public bool IsWordsToStudyShown
        {
            get => _isWordsToStudyShown;
            set
            {
                if (_isWordsToStudyShown == value)
                    return;
                _isWordsToStudyShown = value;

                AdjustWordsToStudyWindowVisibility();

                OnPropertyChanged();
            }
        }

        public bool IsWordsToStudyTransparent
        {
            get => _wordsToStudyWindowManager.ViewModel.IsTransparent;
            set
            {
                if (_wordsToStudyWindowManager.ViewModel != null)
                    _wordsToStudyWindowManager.ViewModel.IsTransparent = value;

                OnPropertyChanged();
            }
        }

        public bool IsClipboardHooked
        {
            get => _generalSettings.Settings.HookClipboard;
            set
            {
                _generalSettings.Settings.HookClipboard = value;
                OnPropertyChanged();
            }
        }

        private void OnWordsToStudyWindowClosed()
        {
            IsWordsToStudyShown = false;
        }

        private void AdjustWordsToStudyWindowVisibility()
        {
            if (_isWordsToStudyShown)
                _wordsToStudyWindowManager.Open();
            else
                _wordsToStudyWindowManager.TryClose();
        }

        private void OnExit()
        {
            _wordsToStudyWindowManager.WindowClosed -= OnWordsToStudyWindowClosed;
            _wordsToStudyWindowManager.Dispose();

            _saveAllSettingsService.SaveAll();
            _shutdownService.Shutdown();
        }
    }
}