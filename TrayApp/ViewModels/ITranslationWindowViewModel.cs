﻿using System;
using System.Windows.Input;
using TrayApp.ViewModels.Common;

namespace TrayApp.ViewModels
{
    public interface ITranslationWindowViewModel : IWindowViewModelBase, IDisposable
    {
        string WordToTranslate { get; set; }

        ITranslatedWordViewModel[] TranslatedWords { get; }

        bool IsLoading { get; }

        string LoadingError { get; }

        ICommand AddWordToStudyListCommand { get; }
    }
}