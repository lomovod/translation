﻿namespace TrayApp.ViewModels
{
    public interface ITranslatedWordViewModel
    {
        string Word { get; }

        bool Clickable { get; }
    }
}