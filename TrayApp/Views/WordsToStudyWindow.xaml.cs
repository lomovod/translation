﻿using System;
using TrayApp.Services.Windows;
using TrayApp.ViewModels;

namespace TrayApp.Views
{
    public partial class WordsToStudyWindow : IWindow<IWordsToStudyViewModel>
    {
        public WordsToStudyWindow()
        {
            InitializeComponent();
        }

        public event Action WindowClosed;

        protected override void OnClosed(EventArgs e)
        {
            WindowClosed?.Invoke();
            base.OnClosed(e);
        }
    }
}