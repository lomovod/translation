﻿using System;
using System.Windows;
using TrayApp.Services.Windows;
using TrayApp.ViewModels;

namespace TrayApp.Views
{
    public partial class UserInputTranslatorView : IWindow<IUserInputTranslatorViewModel>
    {
        public UserInputTranslatorView()
        {
            InitializeComponent();
            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            TextBox.Focus();
        }

        public event Action WindowClosed;

        protected override void OnClosed(EventArgs e)
        {
            WindowClosed?.Invoke();
            base.OnClosed(e);
        }
    }
}