﻿using System;
using TrayApp.Services.Windows;
using TrayApp.ViewModels;

namespace TrayApp.Views
{
    public partial class TranslationWindow : IWindow<ITranslationWindowViewModel>
    {
        public TranslationWindow()
        {
            InitializeComponent();
        }

        public event Action WindowClosed;

        protected override void OnClosed(EventArgs e)
        {
            WindowClosed?.Invoke();
            base.OnClosed(e);
        }
    }
}