﻿using Autofac;
using NLog;
using NLog.Config;
using NLog.Targets;
using TrayApp.Autofac.Modules;
using TrayApp.Services;

namespace TrayApp.Autofac
{
    public static class Bootstrapper
    {
        static Bootstrapper()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule(new MainModule());

            Container = containerBuilder.Build();

            Initialize();

            SetupNLog();
        }

        public static IContainer Container { get; }

        private static void Initialize()
        {
            Container.Resolve<IUserInputTranslatorRegistrationService>().Register();
        }

        private static void SetupNLog()
        {
            var config = new LoggingConfiguration();
            var coloredConsoleTarget = new ColoredConsoleTarget();
            config.AddTarget("console", coloredConsoleTarget);
            var rule = new LoggingRule("*", LogLevel.Trace, coloredConsoleTarget);
            config.LoggingRules.Add(rule);

            LogManager.Configuration = config;
        }
    }
}