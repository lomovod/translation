﻿using Autofac;
using Translator.Api;
using Translator.Client;
using Translator.Common.Services;
using TrayApp.Autofac.Services;
using TrayApp.Domain;
using TrayApp.Services;
using TrayApp.Services.Persistence;
using TrayApp.ViewModels;
using TrayApp.Views;

namespace TrayApp.Autofac.Modules
{
    public class MainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Resolver>().As<IResolver>().SingleInstance();
            builder.RegisterGeneric(typeof(Factory<>)).As(typeof(IFactory<>)).InstancePerDependency().ExternallyOwned();
            builder.RegisterGeneric(typeof(Factory<,>))
                .As(typeof(IFactory<,>))
                .InstancePerDependency()
                .ExternallyOwned();
            builder.RegisterGeneric(typeof(SettingsPersistenceService<>))
                .As(typeof(ISettingsPersistenceService<>))
                .SingleInstance();

            builder.RegisterType<MainWindowViewModel>()
                .As<IMainWindowViewModel>()
                .InstancePerDependency()
                .ExternallyOwned();
            builder.RegisterType<ShutdownService>().As<IShutdownService>().SingleInstance();

            builder.RegisterType<TranslationWindow>()
                .AsImplementedInterfaces()
                .InstancePerDependency()
                .ExternallyOwned();
            builder.RegisterType<UserInputTranslatorView>()
                .AsImplementedInterfaces()
                .InstancePerDependency()
                .ExternallyOwned();

            builder.RegisterType<TranslationWindowViewModel>()
                .As<ITranslationWindowViewModel>()
                .InstancePerDependency()
                .ExternallyOwned();
            builder.RegisterType<TranslatedWordViewModel>()
                .As<ITranslatedWordViewModel>()
                .InstancePerDependency()
                .ExternallyOwned();
            builder.RegisterType<UserInputTranslatorViewModel>()
                .As<IUserInputTranslatorViewModel>()
                .InstancePerDependency()
                .ExternallyOwned();

            builder.RegisterType<WordsToStudyWindow>()
                .AsImplementedInterfaces()
                .InstancePerDependency()
                .ExternallyOwned();
            builder.RegisterType<WordsToStudyViewModel>()
                .As<IWordsToStudyViewModel>()
                .InstancePerDependency()
                .ExternallyOwned();

            builder.RegisterGeneric(typeof(WindowManager<,>)).As(typeof(IWindowManager<,>)).SingleInstance();

            builder.RegisterType<WindowSupervisor>().As<IWindowSupervisor>().SingleInstance();
            builder.RegisterType<TranslatorWindowManager>().As<ITranslatorWindowManager>().SingleInstance();
            builder.RegisterType<TranslationClient>().As<ITranslationClient>().SingleInstance();

            builder.RegisterType<SettingsPersistenceMapper>().As<ISettingsPersistenceMapper>().SingleInstance();
            builder.RegisterType<FolderService>().As<IFolderService>().SingleInstance();
            builder.RegisterType<SettingsStorageExtractor>().As<ISettingsStorageExtractor>().SingleInstance();
            builder.RegisterType<SaveAllSettingsService>().As<ISaveAllSettingsService>().SingleInstance();

            builder.RegisterType<WordsToStudyRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<WordToStudyFactory>().As<IWordToStudyFactory>().SingleInstance();

            builder.RegisterType<GlobalHotkeyService>().As<IGlobalHotkeyService>().SingleInstance();
            builder.RegisterType<UserInputTranslatorRegistrationService>()
                .As<IUserInputTranslatorRegistrationService>()
                .SingleInstance();
        }
    }
}