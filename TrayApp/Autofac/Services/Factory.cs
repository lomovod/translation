﻿using Autofac;

namespace TrayApp.Autofac.Services
{
    internal class Factory<T> : IFactory<T>
    {
        private readonly IResolver _resolver;

        public Factory(IResolver resolver)
        {
            _resolver = resolver;
        }

        public T Create()
        {
            return _resolver.Resolve<T>();
        }
    }

    internal class Factory<TParam, T> : IFactory<TParam, T>
    {
        private readonly IResolver _resolver;

        public Factory(IResolver resolver)
        {
            _resolver = resolver;
        }

        public T Create(TParam param)
        {
            return _resolver.Resolve<T>(TypedParameter.From(param));
        }
    }
}