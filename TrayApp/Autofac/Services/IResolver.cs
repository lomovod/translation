﻿using System;
using Autofac.Core;

namespace TrayApp.Autofac.Services
{
    public interface IResolver
    {
        TService Resolve<TService>(params Parameter[] parameters);
        object Resolve(Type serviceType, params Parameter[] parameters);
    }
}