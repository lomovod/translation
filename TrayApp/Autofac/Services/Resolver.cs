﻿using System;
using Autofac;
using Autofac.Core;

namespace TrayApp.Autofac.Services
{
    internal class Resolver : IResolver
    {
        private readonly IComponentContext _componentContext;

        public Resolver(IComponentContext componentContext)
        {
            _componentContext = componentContext;
        }

        public TService Resolve<TService>(params Parameter[] parameters)
        {
            return _componentContext.Resolve<TService>(parameters);
        }

        public object Resolve(Type serviceType, params Parameter[] parameters)
        {
            return _componentContext.Resolve(serviceType, parameters);
        }
    }
}