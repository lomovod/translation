﻿namespace Translator.Api
{
    public class TranslationResults
    {
        public string[] Translations { get; }

        public TranslationResults(string[] translations)
        {
            Translations = translations;
        }
    }
}