﻿using System.Threading.Tasks;

namespace Translator.Api
{
    public interface ITranslationClient
    {
        Task<TranslationResults> TranslateAsync(string wordToTranslate);
    }
}