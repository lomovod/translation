﻿using Autofac.Features.OwnedInstances;
using Moq;
using NUnit.Framework;
using TrayApp.Autofac;
using TrayApp.Autofac.Services;
using TrayApp.Services;
using TrayApp.Services.Windows;
using TrayAppTests.Assets;

namespace TrayAppTests.Services
{
    [TestFixture]
    public class WindowSupervisorTests
    {
        [SetUp]
        public void Setup()
        {
            _resolverMock = new Mock<IResolver>();
            _windowSupervisor = new WindowSupervisor(_resolverMock.Object);
        }

        private Mock<IResolver> _resolverMock;
        private WindowSupervisor _windowSupervisor;

        [Test]
        public void ResolvesWindowByViewModel()
        {
            var expectedWindow = Mock.Of<IWindow<IFakeViewModel>>();
            _resolverMock
                .Setup(resolver => resolver.Resolve<IWindow<IFakeViewModel>>())
                .Returns(expectedWindow);

            var window = _windowSupervisor.CreateWindow(Mock.Of<IFakeViewModel>());

            Assert.AreSame(expectedWindow, window);
        }
    }
}