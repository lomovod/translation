﻿using Moq;
using NUnit.Framework;
using TrayApp.Services;
using TrayApp.Services.Persistence.Settings;
using TrayApp.ViewModels;

namespace TrayAppTests.Services
{
    [TestFixture]
    public class TranslatorWindowManagerTests
    {
        [SetUp]
        public void SetUp()
        {
            _translatorWindowViewModelMock = new Mock<ITranslationWindowViewModel>();
            _windowManagerMock =
                new Mock<IWindowManager<TranslationWindowViewModelSettings, ITranslationWindowViewModel>>();
            _windowManagerMock.Setup(manager => manager.ViewModel).Returns(_translatorWindowViewModelMock.Object);
        }

        private Mock<ITranslationWindowViewModel> _translatorWindowViewModelMock;

        private Mock<IWindowManager<TranslationWindowViewModelSettings, ITranslationWindowViewModel>> _windowManagerMock
            ;

        private TranslatorWindowManager CreateManager()
        {
            return new TranslatorWindowManager(_windowManagerMock.Object);
        }

        [Test]
        public void ClosesWindowWhenDisposed()
        {
            var manager = CreateManager();
            manager.ShowTranslation("word");

            manager.Dispose();

            _windowManagerMock.Verify(window => window.TryClose());
        }

        [Test]
        public void CreatesWindowIfNotCreated()
        {
            var manager = CreateManager();

            manager.ShowTranslation("word");

            _windowManagerMock.Verify(windowManager => windowManager.Open(),
                Times.Once);
        }

        [Test]
        public void SetsWordDespiteOfWindowCreation()
        {
            const string expectedWord = "anotherWord";
            var manager = CreateManager();

            manager.ShowTranslation("firstWord");
            manager.ShowTranslation(expectedWord);

            _translatorWindowViewModelMock.VerifySet(model => model.WordToTranslate = expectedWord);
        }

        [Test]
        public void SetsWordToViewModel()
        {
            var manager = CreateManager();

            const string wordToTranslate = "word";
            manager.ShowTranslation(wordToTranslate);

            _translatorWindowViewModelMock.VerifySet(model => model.WordToTranslate = wordToTranslate);
        }

        [Test]
        public void ShowsWindowOnCreated()
        {
            var manager = CreateManager();

            manager.ShowTranslation("word");

            _windowManagerMock.Verify(window => window.Open());
        }
    }
}