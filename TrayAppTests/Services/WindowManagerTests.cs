﻿using Moq;
using NUnit.Framework;
using TrayApp.Autofac.Services;
using TrayApp.Services;
using TrayApp.Services.Persistence;
using TrayApp.Services.Persistence.Settings;
using TrayApp.Services.Windows;
using TrayApp.ViewModels;

namespace TrayAppTests.Services
{
    [TestFixture]
    public class WindowManagerTests
    {
        [SetUp]
        public void SetUp()
        {
            _windowSupervisorMock = new Mock<IWindowSupervisor>();
            _translatorWindowViewModelMock = new Mock<ITranslationWindowViewModel>();
            _windowMock = new Mock<IWindow<ITranslationWindowViewModel>>();
            _windowSupervisorMock
                .Setup(supervisor => supervisor.CreateWindow(_translatorWindowViewModelMock.Object))
                .Returns(_windowMock.Object);
            _translationWindowViewModelFactoryMock =
                new Mock<IFactory<TranslationWindowViewModelSettings, ITranslationWindowViewModel>>();
            _translationWindowViewModelFactoryMock
                .Setup(factory => factory.Create(It.IsAny<TranslationWindowViewModelSettings>()))
                .Returns(_translatorWindowViewModelMock.Object);
            _settingsServiceMock = new Mock<ISettingsPersistenceService<TranslationWindowViewModelSettings>>();
        }

        private Mock<IWindowSupervisor> _windowSupervisorMock;
        private Mock<ITranslationWindowViewModel> _translatorWindowViewModelMock;
        private Mock<IWindow<ITranslationWindowViewModel>> _windowMock;

        private Mock<IFactory<TranslationWindowViewModelSettings, ITranslationWindowViewModel>>
            _translationWindowViewModelFactoryMock;

        private Mock<ISettingsPersistenceService<TranslationWindowViewModelSettings>> _settingsServiceMock;

        private WindowManager<TranslationWindowViewModelSettings, ITranslationWindowViewModel> CreateManager()
        {
            return new WindowManager<TranslationWindowViewModelSettings, ITranslationWindowViewModel>(
                _windowSupervisorMock.Object,
                _settingsServiceMock.Object,
                _translationWindowViewModelFactoryMock.Object);
        }

        [Test]
        public void ClosesWindowWhenDisposed()
        {
            var manager = CreateManager();
            manager.Open();

            manager.Dispose();

            _windowMock.Verify(window => window.Close());
        }

        [Test]
        public void ClosesWindowWhenTryToClose()
        {
            var manager = CreateManager();
            manager.Open();

            manager.TryClose();

            _windowMock.Verify(window => window.Close());
        }

        [Test]
        public void CreatesWindowIfNotCreated()
        {
            var manager = CreateManager();

            manager.Open();

            _windowSupervisorMock.Verify(supervisor => supervisor.CreateWindow(_translatorWindowViewModelMock.Object),
                Times.Once);
        }

        [Test]
        public void DestroysAndNullsViewModelWhenWindowClosed()
        {
            var manager = CreateManager();
            manager.Open();
            _windowSupervisorMock.ResetCalls();

            _windowMock.Raise(window => window.WindowClosed += null);
            manager.Open();

            _translatorWindowViewModelMock.Verify(model => model.Dispose());
            _windowSupervisorMock.Verify(supervisor => supervisor.CreateWindow(_translatorWindowViewModelMock.Object));
        }

        [Test]
        public void DoesNotCreateWindowIfViewModelAlreadyCreated()
        {
            var manager = CreateManager();

            manager.Open();
            _windowSupervisorMock.ResetCalls();

            manager.Open();
            _windowSupervisorMock.Verify(model => model.CreateWindow(It.IsAny<ITranslationWindowViewModel>()),
                Times.Never);
        }

        [Test]
        public void DoesNotThrowExceptionIfWindowAlreadyClosed()
        {
            var manager = CreateManager();
            manager.Open();

            manager.TryClose();
            Assert.DoesNotThrow(() => manager.TryClose());
        }

        [Test]
        public void DoesNotThrowExceptionOnDisposedIfWindowDoesnExist()
        {
            var manager = CreateManager();

            Assert.DoesNotThrow(() => manager.Dispose());
        }

        [Test]
        public void ShowsWindowOnCreated()
        {
            var manager = CreateManager();

            manager.Open();

            _windowMock.Verify(window => window.Show());
        }

        [Test]
        public void UnsubscribesFromWindowWhenItsClosed()
        {
            var manager = CreateManager();
            manager.Open();
            _windowMock.Raise(window => window.WindowClosed += null);

            _windowSupervisorMock
                .Setup(supervisor => supervisor.CreateWindow(It.IsAny<ITranslationWindowViewModel>()))
                .Returns(Mock.Of<IWindow<ITranslationWindowViewModel>>());
            manager.Open();
            _windowSupervisorMock.ResetCalls();

            _windowMock.Raise(window => window.WindowClosed += null);
            manager.Open();

            _windowSupervisorMock.Verify(supervisor => supervisor.CreateWindow(It.IsAny<ITranslationWindowViewModel>()),
                Times.Never);
        }
    }
}