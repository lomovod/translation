﻿using Moq;
using NUnit.Framework;
using TrayApp.Services;
using TrayApp.Services.Persistence;
using TrayApp.Services.Persistence.Settings;
using TrayApp.ViewModels;

namespace TrayAppTests.ViewModels
{
    [TestFixture]
    public class MainWindowViewModelTests
    {
        [SetUp]
        public void Setup()
        {
            _shutdownServiceMock = new Mock<IShutdownService>();
            _translatorWindowManagerMock = new Mock<ITranslatorWindowManager>();
            _saveAllSettingsServiceMock = new Mock<ISaveAllSettingsService>();
            _wordsToStudyWindowManagerMock =
                new Mock<IWindowManager<WordsToStudyViewModelSettings, IWordsToStudyViewModel>>();

            _settingsPersistenceServiceMock = new Mock<ISettingsPersistenceService<GeneralSettings>>();
            _settingsPersistenceServiceMock.Setup(service => service.Settings)
                .Returns(new GeneralSettings {HookClipboard = true});

            _mainWindowViewModel = new MainWindowViewModel(_shutdownServiceMock.Object,
                _translatorWindowManagerMock.Object,
                _saveAllSettingsServiceMock.Object,
                _wordsToStudyWindowManagerMock.Object,
                _settingsPersistenceServiceMock.Object);
        }

        private Mock<IShutdownService> _shutdownServiceMock;
        private MainWindowViewModel _mainWindowViewModel;
        private Mock<ITranslatorWindowManager> _translatorWindowManagerMock;
        private Mock<ISaveAllSettingsService> _saveAllSettingsServiceMock;

        private Mock<IWindowManager<WordsToStudyViewModelSettings, IWordsToStudyViewModel>>
            _wordsToStudyWindowManagerMock;

        private Mock<ISettingsPersistenceService<GeneralSettings>> _settingsPersistenceServiceMock;


        [Test]
        public void SavesAllSettingsOnShutdown()
        {
            _mainWindowViewModel.ExitCommand.Execute(null);

            _saveAllSettingsServiceMock.Verify(service => service.SaveAll(), Times.Once);
        }

        [Test]
        public void SetsWordToTranslatorManagerWhenClipboardChanged()
        {
            const string expectedWord = "word";
            _mainWindowViewModel.OnClipboardChanged(expectedWord);

            _translatorWindowManagerMock.Verify(manager => manager.ShowTranslation(expectedWord));
        }

        [Test]
        public void ShutdownsAppOnShutdownCommand()
        {
            _mainWindowViewModel.ExitCommand.Execute(null);

            _shutdownServiceMock.Verify(service => service.Shutdown(), Times.Once);
        }
    }
}