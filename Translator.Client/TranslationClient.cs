﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Translator.Api;

namespace Translator.Client
{
    public class TranslationClient : ITranslationClient
    {
        private const string ServerAddress = "http://127.0.0.1:5454";

        public async Task<TranslationResults> TranslateAsync(string wordToTranslate)
        {
            using (var httpClient = CreateHttpClient())
            {
                var responseMessage = await httpClient.GetAsync($"translate/{wordToTranslate}");
                if (!responseMessage.IsSuccessStatusCode)
                    return null;

                return await responseMessage.Content.ReadAsAsync<TranslationResults>();
            }
        }

        private static HttpClient CreateHttpClient()
        {
            var httpClient = new HttpClient {BaseAddress = new Uri(ServerAddress)};

            httpClient.DefaultRequestHeaders.Clear();
            return httpClient;
        }
    }
}