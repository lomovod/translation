﻿using System;
using System.IO;

namespace Translator.Common.Services
{
    public abstract class FolderServiceBase : IFolderService
    {
        private const Environment.SpecialFolder ApplicationData = Environment.SpecialFolder.ApplicationData;

        public string GetAppFolder(string subPath)
        {
            var folderPath = Environment.GetFolderPath(ApplicationData);
            var combinedPath = Path.Combine(folderPath, RootAppFolder);

            var appFolder = Path.Combine(combinedPath, subPath);
            Directory.CreateDirectory(appFolder);

            return appFolder;
        }

        protected abstract string RootAppFolder { get; }
    }
}