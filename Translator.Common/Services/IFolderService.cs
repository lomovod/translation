﻿namespace Translator.Common.Services
{
    public interface IFolderService
    {
        string GetAppFolder(string subPath);
    }
}